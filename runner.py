#! /usr/bin/env python
import sys
from autohighlight import AutoHighlight

filename = sys.argv[-1]
key = 'standard grey'

highlighter = AutoHighlight(filename, key)
highlighter.run()
highlighter.finalize()
