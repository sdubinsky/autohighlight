# get the OCR string for one frame every half-second
from vapoursynth import core
import vapoursynth as vs
import sys
import json
import datetime
from frame import Frame
# replace any words below the threshold with blanks
def ocr_filter(string, confidence):
    words = string.split()
    for i in range(len(words)):
        if len(words) == 1:
            if confidence < 75:
                words[i] = ''
        elif confidence[i] < 75:
            words[i] = ''
    return "".join(words).strip()

def name_pipeline(clip, threshold, width, height, x, y):
    return (
        clip.
        std.CropAbs(width, height, x, y).
        std.Invert().
        std.Binarize(threshold)
        .std.ShufflePlanes(planes=0, colorfamily=vs.GRAY)
        .resize.Spline36(format=vs.RGB24)
        .ocr.Recognize(language='eng', oem=2)
    )

def number_pipeline(clip, threshold, width, height, x, y):
    return (
        clip.std.CropAbs(width, height, x, y).
        std.Invert().
        std.Binarize(number_threshold).
        std.ShufflePlanes(planes=0, colorfamily=vs.GRAY)
        .resize.Spline36(format=vs.RGB24)
        .ocr.Recognize(language='eng', oem=0, options=['tessedit_char_whitelist', '0123456789', 'tessedit_pageseg_mode', '7'])
    )

def valid_frame(lname, rname, lnumber, rnumber):
    return (
        not lnumber == -1 and
        not rnumber == -1 and
        not leftname and
        not rname
    )

print(datetime.datetime.now())
filename = 'PisteFinaleshort.mkv'
print(datetime.datetime.now())

clip = core.bs.VideoSource(source=filename, hwdevice='cuda')

key = 'kabcom 3'
with open('dimensions.json', 'r') as f:
    dimensions = json.load(f)[str(clip.width)][key]
frame_spacing = clip.fps.numerator / 2
size = clip.width
clip = clip[::int(frame_spacing)]
frame_count = len(clip)
lname_dms = dimensions['lname_dms']
name_threshold = dimensions['name_threshold'] * 100
number_threshold = dimensions['number_threshold'] * 100

lname_clips = name_pipeline(clip, name_threshold, lname_dms[2], lname_dms[3], lname_dms[0], lname_dms[1])

rname_dms = dimensions['rname_dms']
rname_clips = name_pipeline(clip, name_threshold, rname_dms[2], rname_dms[3], rname_dms[0], rname_dms[1])

lnumber_dms = dimensions['lnumber_dms']
lnumber_clips = number_pipeline(clip, number_threshold, lnumber_dms[2], lnumber_dms[3], lnumber_dms[0], lnumber_dms[1])

rnumber_dms = dimensions['rnumber_dms']
rnumber_clips = number_pipeline(clip, number_threshold, rnumber_dms[2], rnumber_dms[3], rnumber_dms[0], rnumber_dms[1])

scores = []

for frame in range(frame_count):
    if frame % 100 == 0:
        print("processing frame " + str(frame))
    # note that confidence is an array where the position in the array
    # is the confidence of the word in that position in the string
    lname = lname_clips.get_frame(frame).props.OCRString
    rname = rname_clips.get_frame(frame).props.OCRString
    if not lname or not rname:
        # if there's a blank string, there won't be an OCRConfidence
        # attr at all
        continue
    lconf = lname_clips.get_frame(frame).props.OCRConfidence
    rconf = rname_clips.get_frame(frame).props.OCRConfidence
    lnumber = lnumber_clips.get_frame(frame).props.OCRString
    rnumber = rnumber_clips.get_frame(frame).props.OCRString
    lname = ocr_filter(lname, lconf)
    rname = ocr_filter(rname, rconf)
    if not lname or not rname:
        continue

    this_frame = Frame(frame, lname, rname, lnumber, rnumber)

    if not scores:
        scores.append(this_frame)
        continue

    last_frame = scores[-1]
    if this_frame != last_frame:
        scores.append(this_frame)

print(scores)
