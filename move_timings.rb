require 'fileutils'

#To use:
#`bundle exec ruby timediff files_to_change`
#works on multiple files.
def update_line(line, timediff)
  newline = line.split(" ")
  oldtime = newline[6].to_i
  newtime = oldtime + timediff
  newline[6] = newtime.to_s
  newline.join " "
end

ARGV[1..-1].map do |file|
  FileUtils.copy file, file + ".bak"
  File.open(file + ".bak", "r") do |f|
    File.open(file, "w+") do |newfile|
      f.each_line do |line|
        newfile.puts update_line line, ARGV[0].to_i
      end
    end
  end
end
