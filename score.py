class Score:
    def __init__(self, frame, touch, fps):
        self.clip_length = 10
        self.lookback_buffer = 1
        self.frame = frame
        self.touch = touch
        self.start_time = int((frame.frame / fps) - self.clip_length - self.lookback_buffer)

    def __str__(self):
        return "{start:08}_{self.frame.lname}_{self.frame.lnumber}_{self.frame.rname}_{self.frame.rnumber}_{self.clip_length}_{self.touch}".format(self=self, start=self.start_time)

    def to_log(self):
        return "time: {:08} | FOTL: {} | score: {} | FOTR: {} | score: {} | touch side: {}".format(self.start_time + self.lookback_buffer + self.clip_length, self.frame.lname, self.frame.lnumber, self.frame.rname, self.frame.rnumber, self.touch)
