from vapoursynth import core
import vapoursynth as vs
import sys
import os
import re
import json
import itertools
import datetime
import logging
from frame import Frame
from score import Score

logger = logging.getLogger(__name__)
class AutoHighlight:
    def __init__(self, filename, key):
        self.filename = filename
        self.prefix = re.sub('[^A-Za-z0-9]', "", filename[0:-4]).lower()
        self.logfile = self.prefix + ".log"
        logging.basicConfig(filename=self.logfile, level=logging.INFO,
                            format='%(asctime)s: %(message)s')
        self.reel_name = self.prefix + "_highlights.mkv"
        self.output_dir = self.prefix + "_output"
        self.key = key
        self.post_clip_buffer = 4 # seconds to wait to see if it's a double
        self.ocr_min_conf = 70
        self.fps = 1

    def make_dir(self):
        if not os.path.exists(self.output_dir):
            os.mkdir(self.output_dir)

    def set_clip(self):
        try:
            self.clip = core.bs.VideoSource(source=self.filename, hwdevice='cuda', cachepath="/home/deus-ex/code/autohighlight/.vps_cache/")
        except:
            # cuda can't handle e.g. webm videos
            self.clip = core.bs.VideoSource(source=self.filename, cachepath="/home/deus-ex/code/autohighlight/.vps_cache/")
        size = self.clip.width
        clip_fps = self.clip.fps.numerator / self.clip.fps.denominator
        logger.info("clip fps: " + str(clip_fps))
        self.frame_spacing = int(clip_fps / self.fps)
        logger.info("frame spacing: " + str(self.frame_spacing))
        self.clip = self.clip[::self.frame_spacing]
        self.clip = self.clip.std.ShufflePlanes(planes=0, colorfamily=vs.GRAY).resize.Spline36(format=vs.RGB24)
        with open('dimensions.json') as f:
            self.dims = json.load(f)[str(self.clip.width)][self.key]
        self.scores = []
        lname_dms = self.dims['lname_dms']
        rname_dms = self.dims['rname_dms']
        lnumber_dms = self.dims['lnumber_dms']
        rnumber_dms = self.dims['rnumber_dms']
        #the prise de fer stream has different overlay colors for left and right
        try:
            lname_threshold = self.dims['lname_threshold'] * 100
            rname_threshold = self.dims['rname_threshold'] * 100
        except:
            lname_threshold = rname_threshold = self.dims['name_threshold'] * 100
        number_threshold = self.dims['number_threshold'] * 100
        try:
            negate_names = self.dims['negate_names']
        except:
            negate_names = False

        try:
            negate_numbers = self.dims['negate_numbers']
        except:
            negate_numbers = False


        self.name_clips = self.name_pipeline(self.clip, lname_threshold, lname_dms[0], lname_dms[1], rname_dms[0], rname_dms[1], lname_dms[2], lname_dms[3], negate_names)
        self.number_clips = self.number_pipeline(self.clip, number_threshold, lnumber_dms[0], lnumber_dms[1], rnumber_dms[0], rnumber_dms[1], lnumber_dms[2], lnumber_dms[3], negate_numbers)

    # replace any words below the threshold with blanks
    def ocr_filter(self, string, confidence):
        words = string.split()
        try:
            for i in range(len(words)):
                if not isinstance(confidence, list):
                    if confidence < self.ocr_min_conf:
                        words = ['']
                        break
                if not confidence:
                    words = ['']
                    break
                elif confidence[i] < self.ocr_min_conf:
                    words[i] = ''
        except Exception as e:
            "error ocr filtering for " + self.filename
        return "".join(words).strip()

    def name_pipeline(self, clip, threshold, left_x, left_y, right_x, right_y, width, height,  negate):
        new_right_x = right_x - left_x
        new_right_y = right_y - left_y
        img_width = (right_x + width) - (left_x)

        clip = clip.std.CropAbs(img_width, height, left_x, left_y)
        if negate:
            clip = clip.std.Invert()
        return (
            clip
            .std.Binarize(threshold)
            .ocr.Recognize(language='eng', oem=1, left_x=0, left_y=0, right_x=new_right_x, right_y=0, width=width, height=height)
        )

    def number_pipeline(self, clip, threshold, left_x, left_y, right_x, right_y, width, height, negate):
        new_right_x = right_x - left_x
        new_right_y = right_y - left_y
        img_width = (right_x + width) - (left_x)

        clip = clip.std.CropAbs(img_width, height, left_x, left_y)
        if negate:
            clip = clip.std.Invert()

        return (
            clip
            .std.Binarize(threshold)
            .ocr.Recognize(language='eng', oem=1, left_x=0, left_y=0, right_x=new_right_x, right_y=0, width=width, height=height, options=['tessedit_char_whitelist', '0123456789', 'tessedit_pageseg_mode', '7', 'debug_file', '/dev/null'])
        )

    def frame_to_frame(self, i):
        # note that confidence is an array where the position in the array
        # is the confidence of the word in that position in the string
        lname = self.name_clips.get_frame(i).props.OCRStringLeft
        rname = self.name_clips.get_frame(i).props.OCRStringRight
        lnumber = self.number_clips.get_frame(i).props.OCRStringLeft
        rnumber = self.number_clips.get_frame(i).props.OCRStringRight
        if (not lname or not rname or not lnumber or not rnumber):
            # if there's a blank string, there
            # won't be an OCRConfidence attr at all
            return
        lname_conf = self.name_clips.get_frame(i).props.OCRConfidenceLeft
        rname_conf = self.name_clips.get_frame(i).props.OCRConfidenceRight
        lnumber_conf = self.number_clips.get_frame(i).props.OCRConfidenceLeft
        rnumber_conf = self.number_clips.get_frame(i).props.OCRConfidenceRight
        lname = self.ocr_filter(lname, lname_conf)
        lname = re.sub('[^A-Za-z]', '', lname)
        rname = self.ocr_filter(rname, rname_conf)
        rname = re.sub('[^A-Za-z]', '', rname)
        lnumber = self.ocr_filter(lnumber, lnumber_conf)
        rnumber = self.ocr_filter(rnumber, rnumber_conf)
        if (not lname or not rname or not lnumber or not rnumber):
            return
        # frame gets the frame spacing number
        return Frame(i, lname, rname, int(lnumber), int(rnumber))

    def record_touch(self, frame, side):
        if not self.scores:
            self.scores.append(Score(frame, side, self.fps))
            logger.info("new score " + self.scores[-1].to_log())
            return

        prev = self.scores[-1]
        # correcting for a late double touch
        if ((frame.frame - (self.post_clip_buffer * self.fps) < prev.frame.frame) and
            ((prev.frame.lnumber + 1 == frame.lnumber and
             prev.touch == 'right') or
            (prev.frame.rnumber + 1 == frame.rnumber and
             prev.touch == 'left'))):
            prev.touch = 'double'
            prev.frame.lnumber = frame.lnumber
            prev.frame.rnumber = frame.rnumber
            logger.info("correcting touch")
        else:
            self.scores.append(Score(frame, side, self.fps))
            logger.info("new score " + self.scores[-1].to_log())

    def run(self):
        if os.path.exists(self.logfile):
            with open(self.logfile, 'r') as f:
                log = f.read()
                if "done finding touches" in log:
                    logger.info("already finished")
                    return
        logger.info("Indexing " + self.filename)
        self.make_dir()
        self.set_clip()
        logger.info("Processing " + self.filename)
        logger.info(str(self.clip.num_frames) + " frames")
        logger.info("width: " + str(self.clip.width))
        prev_frame = None
        for b in itertools.batched(range(self.clip.num_frames - 1), 100):
            batch = list(b)
            logger.info("starting batch " + str(batch[0]))
            first = self.frame_to_frame(batch[0])
            last = self.frame_to_frame(batch[-1])
            if first and last and (first == last):
                logger.info("skipping batch")
                continue

            for i in batch:
                this_frame = self.frame_to_frame(i)
                if not this_frame:
                    continue
                if not prev_frame:
                    prev_frame = this_frame
                    continue
                #scoreboard reset
                if this_frame.lnumber == 0 and this_frame.rnumber == 0:
                    prev_frame = this_frame
                    continue
                if (this_frame.lnumber == prev_frame.lnumber + 1 and
                    this_frame.rnumber == prev_frame.rnumber + 1):
                    self.record_touch(this_frame, 'double')
                elif this_frame.rnumber > prev_frame.rnumber:
                    self.record_touch(this_frame, 'right')
                elif this_frame.lnumber > prev_frame.lnumber:
                    self.record_touch(this_frame, 'left')
                prev_frame = this_frame
        logger.info("done finding touches")
        self.write_scores()
        self.write_calls()

    def write_calls(self):
        with open(self.prefix + "_calls.sh", 'w') as f:
            for score in self.scores:
                # one frame every two seconds means the clip starts
                # two*current frame seconds
                call = "ffmpeg -y -nostdin -loglevel 24 -ss {start} -i '{self.filename}' -t {score.clip_length} -avoid_negative_ts 1 -c copy '{self.output_dir}/{score}.mkv'\n".format(score=score, self=self, start=score.start_time)
                f.write(call)

    def write_scores(self):
        with open(self.prefix + "_scores.txt", 'w') as f:
            for score in self.scores:
                f.write(score.to_log() + '\n')

    def write_file_list(self):
        with open(self.prefix + "_files.txt", 'w') as f:
            for clip in sorted(os.listdir(self.output_dir)):
                f.write("file " + "'" + self.output_dir + "/" + clip + "'" + "\n")

    def finalize(self):
        os.system('bash ' + self.prefix + "_calls.sh")
        self.write_file_list()
        os.system("ffmpeg -y -nostdin -loglevel 24 -f concat -safe 0 -i {prefix}_files.txt -c copy '{reel_name}'".format(prefix=self.prefix, reel_name = self.prefix + '_highlights.mkv'))
