from vapoursynth import core
import vapoursynth as vs
import json


key = 'kabcom 3'
with open('dimensions.json', 'r') as f:
    dimensions = json.load(f)['1920'][key]


clip = core.bs.VideoSource(source='PisteFinaleshort.mkv', hwdevice='cuda')
dims = dimensions['lnumber_dms']

clip = clip[900 * 15]
clip = (
    clip.
    std.CropAbs(dims[2], dims[3], dims[0], dims[1])
    .std.ShufflePlanes(planes=[0], colorfamily=vs.GRAY)
    .resize.Spline36(format=vs.RGB24)
)
clip = clip.fpng.Write(filename="/home/deus-ex/code/autohighlight/ramdisk/test_%d.png", overwrite=True)

clip  = core.ocr.Recognize(clip, language="eng", oem=0, options=['tessedit_pageseg_mode', '7', 'classify_enable_adaptive_matcher', '0', 'tessedit_char_whitelist', '0123456789'])

for frame in clip.frames():
    print(frame.props.OCRString)
