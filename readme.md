# Autohighlight

This program cuts clips from long streams with static overlays based on changes in the overlay.

At a high level, it cuts the stream into frames half a second apart, compares the part of the overlay responsible for scoring between two frames, and runs an ffmpeg call to cut a clip based on that time.

## Dependencies

Ruby, bundler, ffmpeg, imagemagick, minimagick, tesseract,.  Your friendly neighborhood package manager can get you imagemagick, ffmpeg, tesseract, parallel, and bundler, bundler will get you minimagick.  Note that minimagick depends on imagemagick version six only.  Homebrew will allow you to install it using `brew install imagemagick@6`.  Tesseract is for finding the names and scores.

You also have to install a more complete version of the traineddata files.  You can download it from [here](https://github.com/tesseract-ocr/tessdata/blob/master/eng.traineddata) and stick it in `/usr/local/share/tessdata` on a mac, or `/usr/share/tesseract-ocr/5/tessdata/` on Ubuntu.  If you're not sure, remove the `quiet` parameter from the tesseract calls in `frame.rb` run the `process_single_frame.rb` file on a sample frame, and look at the error messages.  It replaces the one already in there.

## Usage

There's a runner file that takes the names of the video files as an argument.  Call it like `ruby runner.rb $VIDEO_FILE $ANOTHER_FILE`.  At some point it'll get a more user-friendly interface.

I recommend creating a RAM disk for mini magick to use.  For that purpose, there is a mac-only script called `ramdisk.sh`.


## Example Usage

1. Install dependencies
2. Run `bundle`
3. Copy the video file to the autohighlight directory
4. Check out the video file in a few places to confirm that the overlay is in the normal place.  `Frame#valid_frame?` contains a number of checks for various parts of the overlay.  Disable any that aren't relevant.  For example, not all videos have a schedule overlay, and not all videos have the Tissot logo present.
5. Run it on one file with `bundle exec ruby runner.rb $FILENAME`.  Check the progress in the log and make sure everything looks normal.  You might need to tweak it several times to get good results.
6. Run it on the rest of the files.
7. Create a file named `files.txt` where each line of the file is of the form "file 'file_highlights.mp4'".  One line per generated highlight video.  Run `ffmpeg -f concat -safe 0 -i files.txt -c copy highlights.mp4`.

## Code Overview

### Parameters
* `filename`: name of the raw video file.
* `frame_dir`: directory that will or does contain the cut frames.
* `output_dir`: directory that will contain the output clips.
* `batch_size`: Number of frames to process at once.
* `clip_length`: length of each clip, in seconds.
* `post_clip_buffer`: number of seconds after each touch within which a score change is considered a double touch.
* `touch_skip_length`: After this many seconds, stop measuring the time between touches.
* `negative_buffer`: Number of seconds to move backwards when clipping.  This is because they usually wait a few seconds before they change the score.
* `reel_name`: The name of the output reel.

### Methods

The key method is `Autohighlight#cut_clips`.  It's basically the `main` method.  It follows these steps:
* `generate_frames`: Cut the input video into one-second frames.  It will skip this if the `@raw_frames` directory is not empty.
* Batch process the frames.  Trying to do them all at once crashed my computer.  If you have more RAM than I do(8gb) you can do more at once, although I don't know what sort of speedup you'll get.
* Create `Frame` objects for each filename in the batch.
* `compare_frames`: This is the real heart of the code.  It does a lot.  Two frames at a time, it does:
  * make each `Frame` object set its variables.  This only happens once per object, even though it's called twice.
  * skip this pair if the first frame is missing the overlay.
  * Check if either number changed.  If they change within `@post_clip_buffer` assume it's a double.  If they changed, end the loop here.
  * check if the right frame's numbers both equal 0.  If so, the scoreboard was reset.
  * Check if the clock changed.  If it did, increment the timer.
  * Check if either number changed again.  If they did, set the skip timer, record the time between this touch and the last touch, record everything.
    * Check if both numbers changed.  If so, it was a double touch.
* `write_clips`: Create the ffmpeg calls based on the recorded touches.
* `write_file_list`: For ffmpeg to concatenate multiple videos into one, it needs a file containing the list of all videos you want to concatenate.  This method generates that file.
* `generate_highlight_reel`: Create the ffmpeg command that merges all the output files together into one.
* `print_stats`: Print the total number of touches, total number of double touches, and average time between touches.

## benchmarking

* using minimagick and imagemagick 6.9, the test file took
real    5m17.147s
user    8m56.318s
sys     1m23.581s
