require 'pry'
require 'json'
require 'excon'
require 'curb'
require 'psych'
require 'fileutils'
require 'aws-sdk-s3'
require 'securerandom'

class GfyUpload
  attr_accessor :gfy_ids
  def initialize
    @gfy_ids = []
    puts "Enter city name: "
    @city = $stdin.gets
    @city.strip!
    puts "Enter weapon: "
    @weapon = $stdin.gets
    @weapon.strip!
    exit "must enter a weapon" unless @weapon != ''
    puts "Enter gender(male/female): "
    @gender = $stdin.gets
    @gender.strip!
    puts "Enter year(default #{Time.now.year}): "
    @year = $stdin.gets
    @year.strip!
    @year = Time.now.year.to_s unless @year != ''
    puts "enter event type (i/t)"
    @event_type = $stdin.gets.strip!
    @event_type = @event_type == 'i' ? "individual" : "team"
    @event = "#{@city}#{@weapon}#{@year}"
    puts @event
    @client = get_aws_client
    @animals = File.readlines("namer/animals.txt")
    @adjectives = File.readlines("namer/adjectives.txt")
    @bucket = 'fencingdatabase'
  end

  def list_objects
    resp = @client.list_objects_v2(bucket: @bucket)
    puts resp.to_h
  end

  def upload_clips clips
    Dir.mkdir "done" unless Dir.exist? "done"
    threads = []
    16.times do
      threads << Thread.new do
        prefix = SecureRandom.uuid
        loop do
          clip = clips.pop
          break unless clip
          begin
            convert_clip clip, prefix
            upload_to_r2 clip, prefix
          rescue => e
            puts e
            next
          end
          FileUtils.mv clip, "done"
        end
      end
    end

    threads.each { |thr| thr.join }
  end

  def convert_clip clip, prefix
    `ffmpeg -y -loglevel 24 -nostdin -i '#{clip}' -c copy /tmp/#{prefix}current.mp4`
  end

  def upload_to_r2 filename, prefix
    begin
      clip_name = filename.split(".")[0].split("/")[-1]
      tag_format = ['time', 'leftname', 'leftscore', 'rightname', 'rightscore', 'clip_length', 'touch', 'tournament', 'gender', 'weapon', 'event_type']
      tags = tag_format.zip(clip_name.split("_") + [@event, @gender, @weapon, @event_type]).to_h
      name = generate_name
      tries = 2
      begin
        File.open("/tmp/#{prefix}current.mp4") do |f|
          response = @client.put_object(
            {
              body: f,
              bucket: @bucket,
              content_type: 'video/mp4',
              key: @event + "/" + name + ".mp4",
              metadata: tags
            })
          @gfy_ids << name
        end
      rescue => e
        puts "general rescue 1"
        puts e
        tries = tries - 1
        retry if tries >= 0
        exit(1)
      end
    end
  end

  def get_aws_client
    secrets = Psych.load_file('r2_secrets.yml')
    url = 'https://4dd32318115734d076dd61a3ace4f5ec.r2.cloudflarestorage.com'
    Aws::S3::Client.new(access_key_id: secrets['access_key'], secret_access_key: secrets['secret_key'], region: 'auto', endpoint: url)
  end

  def generate_name
    p1 = @adjectives.sample.strip
    p2 = @animals.sample.strip
    p3 = @animals.sample.strip
    name = p1 + p2 + p3
    name
  end
end

g = GfyUpload.new
g.upload_clips ARGV
File.open("gfys.txt", "w").write(g.gfy_ids.join("\n"))
