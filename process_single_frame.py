#! /usr/bin/env python
import re
import os
import sys
import json
from frame import Frame
from vapoursynth import core
import vapoursynth as vs
# set to all to check all options
key = 'finals blue'
timestamp = 1708
filename = sys.argv[-1]
def name_pipeline(clip,
             threshold,
             left_x,
             left_y,
             right_x,
             right_y,
             width,
             height,
             invert):

    new_right_x = right_x - left_x
    new_right_y = right_y - left_y
    img_width = (right_x + width) - (left_x)
    clip = clip.std.CropAbs(img_width, height, left_x, left_y)
    if invert:
        clip = clip.std.Invert()
    return (
        clip
        .std.Binarize(threshold)
        .ocr.Recognize(language='eng', oem=1, left_x=0, left_y=0, right_x=new_right_x, right_y=0, width=width, height=height)
    )

def number_pipeline(clip, threshold, left_x, left_y, right_x, right_y, width, height, invert):
    new_right_x = right_x - left_x
    new_right_y = right_y - left_y
    img_width = (right_x + width) - (left_x)
    print([img_width, height, left_x, left_y])
    clip = clip.std.CropAbs(img_width, height, left_x, left_y)

    if invert:
        clip = clip.std.Invert()
    return (
        clip
        .std.Binarize(threshold)
        .ocr.Recognize(language='eng', oem=1, left_x=0, left_y=0, right_x=new_right_x, right_y=0, width=width, height=height, options=['tessedit_char_whitelist', '0123456789', 'tessedit_pageseg_mode', '7', 'debug_file', '/dev/null'])
    )

def as_png(clip):
    return clip.fpng.Write(filename="/home/deus-ex/code/autohighlight/test_%d.png", overwrite=True)

try:
    clip = core.bs.VideoSource(source=filename, hwdevice='cuda', cachepath="/home/deus-ex/code/autohighlight/.vps_cache/")
except:
    clip = core.bs.VideoSource(source=filename, cachepath="/home/deus-ex/code/autohighlight/.vps_cache/")

clip = clip.std.ShufflePlanes(planes=0, colorfamily=vs.GRAY).resize.Spline36(format=vs.RGB24)

print(clip.width)
print(clip.fps.denominator)
print(clip.num_frames)

with open('dimensions.json', 'r') as f:
    dim_options = json.load(f)[str(clip.width)]
clip_fps = int(clip.fps.numerator / clip.fps.denominator)
i = int(timestamp * clip_fps)
clip = clip[i]

def test_dim_option(dims):
    lname_dms = dims['lname_dms']
    rname_dms = dims['rname_dms']
    lnumber_dms = dims['lnumber_dms']
    rnumber_dms = dims['rnumber_dms']
    try:
        lname_threshold = dims['lname_threshold'] * 100
        rname_threshold = dims['rname_threshold'] * 100
    except:
        lname_threshold = rname_threshold = dims['name_threshold'] * 100

    number_threshold = dims['number_threshold'] * 100
    try:
        negate_names = dims['negate_names']
    except:
        negate_names = False

    try:
        negate_numbers = dims['negate_numbers']
    except:
        negate_numbers = False

    name_clips = name_pipeline(clip, lname_threshold, lname_dms[0], lname_dms[1], rname_dms[0], rname_dms[1], lname_dms[2], lname_dms[3], negate_names)
    number_clips = number_pipeline(clip, number_threshold, lnumber_dms[0], lnumber_dms[1], rnumber_dms[0], rnumber_dms[1], lnumber_dms[2], lnumber_dms[3], negate_numbers)

    #name_clips = as_png(name_clips)
    number_clips = as_png(number_clips)
    lname = name_clips.get_frame(0).props.OCRStringLeft
    lnameconf = name_clips.get_frame(0).props.OCRConfidenceLeft if lname else -1
    lname = re.sub('[^A-Za-z ]', '', lname)
    rname = name_clips.get_frame(0).props.OCRStringRight
    rname = re.sub('[^A-Za-z ]', '', rname)
    rnameconf = name_clips.get_frame(0).props.OCRConfidenceRight if rname else -1
    lnumber = number_clips.get_frame(0).props.OCRStringLeft
    lnumberconf = number_clips.get_frame(0).props.OCRConfidenceLeft if lnumber else -1
    rnumber = number_clips.get_frame(0).props.OCRStringRight
    rnumberconf = number_clips.get_frame(0).props.OCRConfidenceRight if rnumber else -1
    print(lname + "(" + str(lnameconf) + ")" " | " + lnumber  + "(" + str(lnumberconf) + ")" + " | " + rname + "(" + str(rnameconf) + ")"  + " | " + rnumber + "(" + str(rnumberconf) + ")" )

if key == "all":
    for key in dim_options.keys():
        print(key)
        test_dim_option(dim_options[key])
        print("--------")
else:
    print(key)
    test_dim_option(dim_options[key])
